package com.nmcint.autoscreenshotcropper;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.view.View.OnClickListener;
import android.widget.TextView;



public class MainActivity extends AppCompatActivity {
    private Button mButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button button = findViewById(R.id.btnSelectFolder);

        button.setOnClickListener(new OnClickListener()
        {
            public void onClick(View v)
            {
                TextView mT = findViewById(R.id.txtResult);
                mT.setText("I am clicked");
            }
        });

    }
}
